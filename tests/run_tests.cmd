rem curl http://localhost:8080/api/getUserRoles
rem curl http://localhost:8080/api/getSystemRoles

rem curl http://localhost:8080/api/getUserRoleRelations/1
rem curl http://localhost:8080/api/getSystemRoleRelations/1


rem curl http://localhost:8080/api/getUserRole/1
rem curl http://localhost:8080/api/getUserRole/100

rem curl http://localhost:8080/api/getSystemRole/1
rem curl http://localhost:8080/api/getSystemRole/100

rem curl http://localhost:8080/api/deleteUserRole/3
rem curl http://localhost:8080/api/deleteSystemRole/3
rem curl http://localhost:8080/api/deleteRoleRelation/3

rem curl -X POST -H "Content-Type: application/json" -d @new_role.json http://localhost:8080/api/updateUserRole
rem curl -X POST -H "Content-Type: application/json" -d @update_role_3.json http://localhost:8080/api/updateUserRole


rem curl -X POST -H "Content-Type: application/json" -d @new_system_role.json http://localhost:8080/api/updateSystemRole
curl -X POST -H "Content-Type: application/json" -d @update_system_role_3.json http://localhost:8080/api/updateSystemRole

rem curl -X POST -H "Content-Type: application/json" -d @new_role_relation.json http://localhost:8080/api/updateRoleRelation
rem curl -X POST -H "Content-Type: application/json" -d @new_role_relation_bad.json http://localhost:8080/api/updateRoleRelation
