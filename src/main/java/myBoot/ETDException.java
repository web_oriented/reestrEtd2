package myBoot;

import java.util.HashMap;
import java.util.Map;

public class ETDException extends Exception {

    //системная, бизнес
    protected int type;

    static final Map<Integer , String> typeDescriptions = new HashMap<Integer , String>() {{
        put(100, "Общая ошибка");
        put(101, "Роль не найдена");
        put(102, "Полномочие не найдено");
        put(103, "Связь Роль-Полномочие не найдена");
        put(104, "Невозможно изменить удаленную роль");
        put(105, "Невозможно изменить удаленное полномочие");
        put(106, "Невозможно изменить удаленную связь Роль-Полномочие");
        put(107, "Невозможно добавить связь Роль-Полномочие. Связь уже существует в системе");


    }};

    protected long code;
    protected String description;

    public ETDException(){

    }

    public ETDException(int type, long code, String description){
        super("type: "+type +": "+ typeDescriptions.get(type)+", code: "+code+". "+description);
        this.type = type;
        this.code = code;
        this.description = description;
    }
}
