package myBoot;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@ComponentScan
@EnableAutoConfiguration
public class main {


    public static void main(String[] args) throws Exception {
        ApplicationContext applicationContext = SpringApplication.run(main.class, args);

       /* for (String name : applicationContext.getBeanDefinitionNames()) {
            System.out.println(name);
        }
        */
    }




    @Controller
    public class StartController {

        @RequestMapping("/start")
        public String welcome() {

            return "start";
        }
    }



}