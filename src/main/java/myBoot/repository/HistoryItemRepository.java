package myBoot.repository;

import myBoot.models.HistoryItem;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface HistoryItemRepository extends CrudRepository<HistoryItem, Long> {
    List<HistoryItem> getAllByUserRoleId(Long userRoleId);
    List<HistoryItem> getAllBySystemRoleId(Long systemRoleId);
    List<HistoryItem> getAllByRoleRelationId(Long systemRoleId);
}