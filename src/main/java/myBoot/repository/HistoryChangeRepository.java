package myBoot.repository;

import myBoot.models.HistoryChange;
import org.springframework.data.repository.CrudRepository;

public interface HistoryChangeRepository extends CrudRepository<HistoryChange, Long> {

}