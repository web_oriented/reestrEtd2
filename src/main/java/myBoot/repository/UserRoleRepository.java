package myBoot.repository;

import myBoot.models.UserRole;
import org.springframework.data.repository.CrudRepository;

public interface UserRoleRepository extends CrudRepository<UserRole, Long> {
    UserRole getByIdAndIsDeleted(Long id, Boolean isDeleted);

    UserRole getFirstByIsDeletedOrderByIdDesc(Boolean isDeleted);
}