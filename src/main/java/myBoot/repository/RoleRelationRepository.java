package myBoot.repository;

import myBoot.models.RoleRelation;
import myBoot.models.SystemRole;
import myBoot.models.UserRole;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface RoleRelationRepository extends CrudRepository<RoleRelation, Long> {
    List<RoleRelation> findRoleRelationBySystemRoleId(long systemRoleId);
    List<RoleRelation> findRoleRelationByUserRoleId(long userRoleId);

    RoleRelation getByUserRoleIdAndSystemRoleIdAndIsDeleted(long userRoleId, long systemRoleId, Boolean isDeleted);

    List<RoleRelation> findRoleRelationsByUserRoleIdAndSystemRoleIdAndIsDeleted(long userRoleId, long systemRoleId, Boolean isDeleted);

    RoleRelation getFirstByIsDeletedOrderByIdDesc(Boolean isDeleted);

}