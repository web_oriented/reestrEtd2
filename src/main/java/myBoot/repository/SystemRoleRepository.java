package myBoot.repository;

import myBoot.models.SystemRole;
import org.springframework.data.repository.CrudRepository;

public interface SystemRoleRepository extends CrudRepository<SystemRole, Long> {
    SystemRole getByIdAndIsDeleted(Long id, Boolean isDeleted);

    SystemRole getFirstByIsDeletedOrderByIdDesc(Boolean isDeleted);
}