package myBoot.models;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Calendar;

@Entity
public class HistoryChange {

    @Getter
    @Setter
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Getter
    @Setter
    @NotNull
    protected String propertyName;


    @Getter
    @Setter
    protected String oldValue;


    @Getter
    @Setter
    protected String newValue;



}

