package myBoot.models;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Calendar;

@Entity
public class RoleRelation extends baseEntity{


    @Getter
    @Setter
    protected Long userRoleId;

    @Getter
    @Setter
    protected Long systemRoleId;


    @Getter
    @Setter
    @NotNull
    protected Boolean inLanitContour = false;

    @Getter
    @Setter
    @NotNull
    protected Boolean inTPAKContour = false;

    @Getter
    @Setter
    @NotNull
    protected Boolean inPPAKContour = false;



    @Getter
    @Setter
    @NotNull
    protected Boolean isDefault = false;




    @Override
    protected String getFieldHRName(String fieldName){
        String fieldHRName=fieldName;
        switch (fieldName) {
            case "inLanitContour":fieldHRName = "в контуре 'Ланит'";break;
            case "inTPAKContour":fieldHRName = "в контуре 'Ланит'";break;
            case "inPPAKContour":fieldHRName = "в контуре 'Ланит'";break;
            case "isDeleted":fieldHRName = "Удалена";break;
            case "isDefault":fieldHRName = "По умолчанию";break;
            case "comment":fieldHRName = "Комментарий";break;
            case "lastChangeDate":fieldHRName = "Изменена";break;
            case "jiraIssue":fieldHRName = "Задача Jira";break;
        }
        return fieldHRName;
    }
}

