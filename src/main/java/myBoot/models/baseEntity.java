package myBoot.models;
/*базовый класс для доступа к protected полям сущности*/

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.*;

@MappedSuperclass
public class baseEntity {

    protected  String getFieldHRName(String fieldName){
        return fieldName;
    }


    public Set<HistoryChange> getChangesSet(baseEntity oldObject, boolean checkSuperClass){


        Set<HistoryChange> resultSet = new HashSet<HistoryChange>();
        if (oldObject == null)
            return resultSet;

        Class<?> thisClass = null;
        try {
            thisClass = Class.forName(this.getClass().getName());


            ArrayList<Field> aClassFields = new ArrayList<Field>();
            for(Field f: thisClass.getDeclaredFields())
                aClassFields.add(f);

            for(Field f: thisClass.getSuperclass().getDeclaredFields())
                aClassFields.add(f);



            Class<Comparable> cls = Comparable.class;
            for (Field f : aClassFields) {
                if (cls.isAssignableFrom(f.getType())) {
                    Comparable c = (Comparable) f.get(this);
                    int r = c.compareTo((Comparable) f.get(oldObject));
                    if (r != 0) {


                        HistoryChange change = new HistoryChange();
                        change.setPropertyName(this.getFieldHRName(f.getName()));
                        if (f.getType()==Calendar.class){
                            change.setOldValue(new SimpleDateFormat("yyyy.MM.dd HH:mm:ss").format(((Calendar)f.get(oldObject)).getTime()));
                            change.setNewValue(new SimpleDateFormat("yyyy.MM.dd HH:mm:ss").format(((Calendar)f.get(this)).getTime()));
                        }
                        else {
                            change.setOldValue(f.get(oldObject).toString());
                            change.setNewValue(f.get(this).toString());
                        }
                        resultSet.add(change);

                        System.out.println(getFieldHRName(f.getName())+ ":"+ f.get(oldObject).toString()+ "->"+ f.get(this).toString());
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }



        return resultSet;
    }



    @Getter
    @Setter
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;


    @Getter
    @Setter
    protected Calendar lastChangeDate;

    @Getter
    @Setter

    @NotNull
    protected Boolean isDeleted = false;

    @Getter
    @Setter
    protected String jiraIssue = "";
}
