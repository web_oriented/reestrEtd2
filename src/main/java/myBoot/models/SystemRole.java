package myBoot.models;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Calendar;


@Entity
public class SystemRole extends baseEntity{





    @Getter
    @Setter
    protected Integer roleNumber ;

    @Getter
    @Setter
    protected String name = "";



    @Getter
    @Setter
    @NotNull
    protected Boolean inLanitContour = false;

    @Getter
    @Setter
    @NotNull
    protected Boolean inTPAKContour = false;

    @Getter
    @Setter
    @NotNull
    protected Boolean inPPAKContour = false;


    @Getter
    @Setter
    protected String comment = "";




    @Override
    protected String getFieldHRName(String fieldName){
        String fieldHRName=fieldName;
        switch (fieldName) {
            case "roleNumber":fieldHRName = "Номер роли";break;
            case "name":fieldHRName = "Наименование";break;
            case "allowBlocking":fieldHRName = "М.б.заблокирована";break;
            case "inLanitContour":fieldHRName = "в контуре 'Ланит'";break;
            case "inTPAKContour":fieldHRName = "в контуре 'Ланит'";break;
            case "inPPAKContour":fieldHRName = "в контуре 'Ланит'";break;
            case "isDeleted":fieldHRName = "Удалена";break;
            case "comment":fieldHRName = "Комментарий";break;
            case "lastChangeDate":fieldHRName = "Изменена";break;
            case "jiraIssue":fieldHRName = "Задача Jira";break;
        }
        return fieldHRName;
    }

}
