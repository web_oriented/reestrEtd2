package myBoot.models;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

@Entity
public class HistoryItem extends baseEntity{


    @Getter
    @Setter
    protected Long userRoleId;

    @Getter
    @Setter
    protected Long systemRoleId;

    @Getter
    @Setter
    protected Long roleRelationId;


    @Getter
    @Setter
    @NotNull
    protected Integer action;



    @Getter
    @Setter
    @OneToMany(cascade={CascadeType.ALL}, fetch = FetchType.LAZY)
    @JoinColumn(name="history_item_id")
    protected Set<HistoryChange> changes;



}

