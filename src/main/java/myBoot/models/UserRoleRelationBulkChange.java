package myBoot.models;

import java.util.ArrayList;
import java.util.List;

public class UserRoleRelationBulkChange {
    public UserRoleRelationBulkChange(){
        deletedRelations = new ArrayList<Long>();
        addedRelations = new ArrayList<RoleRelation>();
    }
    public List<Long> deletedRelations;
    public List<RoleRelation> addedRelations;
}
