package myBoot.controllers;

import myBoot.ETDException;
import myBoot.models.*;
import myBoot.repository.HistoryItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;

import java.util.*;
//@Controller
public class RestControllerBase {

    protected static String myServerURL="http://localhost:8080";



    @Autowired
    HistoryItemRepository historyItemRepository;

    public static enum ACTION {NEW_ELEMENT, MODIFY_ELEMENT, DELETE_ELEMENT};

    protected void SaveHistoryInformation(baseEntity element, ACTION action, Set<HistoryChange> changesSet){



        Long userRoleID = null;
        Long systemRoleID = null;
        Long roleRelationID = null;


        if (element instanceof UserRole)
            userRoleID = element.getId();
        if (element instanceof SystemRole)
            systemRoleID = element.getId();
        if (element instanceof RoleRelation)
            roleRelationID = element.getId();


        HistoryItem historyItem = new HistoryItem();
        historyItem.setUserRoleId(userRoleID);
        historyItem.setSystemRoleId(systemRoleID);
        historyItem.setRoleRelationId(roleRelationID);
        historyItem.setAction(action.ordinal());
        historyItem.setLastChangeDate(Calendar.getInstance());
        historyItem.setJiraIssue(element.getJiraIssue());

        historyItem.setChanges(changesSet);

        historyItemRepository.save(historyItem);
    }




    public <T> List<T> getEntityList(CrudRepository repository,  Class<T> responseType ) throws Exception{
        try {
            List<T> list = new ArrayList<T>();

            for ( Object entity : repository.findAll()) {
                list.add((T)entity);
            }

            return list;
        }
        catch (Exception ex){

            throw new Exception("Error on getEntityList");
        }
    }


    public baseEntity getEntity(CrudRepository repository,  Long entityID, int exceptionType) throws ETDException {

        Optional<baseEntity> result = repository.findById(entityID);
        if (result.isPresent())
            return  result.get();
        else
            throw new ETDException(exceptionType, 00, "Ошибка при получении элемента: не найден");

    }

    public baseEntity saveEntity(CrudRepository repository, baseEntity newElement, int exceptionType)  throws Exception {

        newElement.setLastChangeDate(Calendar.getInstance());
        newElement.setIsDeleted(false);


        baseEntity oldElement = null;

        RestControllerBase.ACTION action = ACTION.NEW_ELEMENT; //создание

        if (newElement.getId()!=null) {
            action = ACTION.MODIFY_ELEMENT; //модификация
            Optional<baseEntity> findResult = repository.findById(newElement.getId());
            if (findResult.isPresent()) {
                oldElement = findResult.get();
                if (oldElement.getIsDeleted() == true)
                    throw new ETDException(exceptionType, 00, "Ошибка при модификации удаленного элемента " + oldElement.getId());
            }
        }

        Set<HistoryChange> changesSet = newElement.getChangesSet(oldElement, true);

        newElement = (baseEntity) repository.save(newElement);


        SaveHistoryInformation(newElement, action,  changesSet);


        return  newElement;
    }

    public baseEntity deleteEntity(CrudRepository repository, Long entityID, int exceptionType ) throws Exception{
        //TO DO - сделать каскадное удаление элементов - удалили роль - удалить связи с ней. записать историю об этом

        Optional<baseEntity> findResult = repository.findById(entityID);
        if (findResult.isPresent()) {
            baseEntity oldElement = findResult.get();
            oldElement.setIsDeleted(true);

            oldElement = (baseEntity)repository.save(oldElement);

            //запись истории о удалении роли
            SaveHistoryInformation(oldElement, ACTION.DELETE_ELEMENT,  null);

            return oldElement;
        }
        else
            throw new ETDException(exceptionType, 00, "Ошибка при удалении элемента: не найден");
    }






}
