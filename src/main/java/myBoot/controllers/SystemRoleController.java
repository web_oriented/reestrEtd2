package myBoot.controllers;

import myBoot.ETDException;
import myBoot.models.HistoryChange;
import myBoot.models.HistoryItem;
import myBoot.models.SystemRole;
import myBoot.models.UserRole;
import myBoot.repository.HistoryItemRepository;
import myBoot.repository.SystemRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@Controller
public class SystemRoleController extends RestControllerBase {


    @Autowired
    SystemRoleRepository systemRoleRepository;


    @RequestMapping(value="/api/getSystemRoles")
    @ResponseBody
    public List<SystemRole> getSystemRoles() throws Exception{
        return getEntityList(systemRoleRepository, SystemRole.class);
    }



    @RequestMapping(value="/api/getSystemRole/{SystemRoleID}")
    @ResponseBody
    public SystemRole getSystemRole(@PathVariable("SystemRoleID") Long SystemRoleID) throws Exception {
        return (SystemRole)getEntity(systemRoleRepository, SystemRoleID, 102);
    }



    @RequestMapping(value="/api/updateSystemRole", method = RequestMethod.POST, headers="Accept=*/*",  produces="application/json;; charset=UTF-8")
    @ResponseBody
    public SystemRole updateSystemRole(@RequestBody SystemRole element)  throws Exception {

        return (SystemRole) saveEntity(systemRoleRepository, element, 105);
    }

    @RequestMapping(value="/api/deleteSystemRole/{SystemRoleID}")
    @ResponseBody
    public SystemRole deleteSystemRole(@PathVariable("SystemRoleID") Long SystemRoleID) throws Exception{
        //TO DO - сделать каскадное удаление элементов - удалили роль - удалить связи с ней. записать историю об этом
        return (SystemRole) deleteEntity(systemRoleRepository, SystemRoleID, 105);
    }

    @RequestMapping(value="/api/getSystemRoleHistory/{SystemRoleID}")
    @ResponseBody
    public List<HistoryItem> getUserRoleHistory(@PathVariable("SystemRoleID") Long systemRoleID) throws Exception{
        return historyItemRepository.getAllBySystemRoleId(systemRoleID);
    }


}
