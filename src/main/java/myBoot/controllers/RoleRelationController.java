package myBoot.controllers;

import myBoot.ETDException;
import myBoot.models.*;
import myBoot.repository.RoleRelationRepository;
import myBoot.repository.SystemRoleRepository;
import myBoot.repository.UserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@Controller
public class RoleRelationController extends RestControllerBase {





    @Autowired
    RoleRelationRepository roleRelationRepository;

    @Autowired
    SystemRoleRepository systemRoleRepository;

    @Autowired
    UserRoleRepository userRoleRepository;





    @RequestMapping(value="/api/getSystemRoleRelations/{SystemRoleID}")
    @ResponseBody
    public List<RoleRelation> getSystemRoleRelations(@PathVariable("SystemRoleID") Long SystemRoleID) throws ETDException {

        List<RoleRelation> list = new ArrayList<RoleRelation>();


        Optional<SystemRole> result = systemRoleRepository.findById(SystemRoleID);
        if (result.isPresent()) {
            for (RoleRelation roleRelation : roleRelationRepository.findRoleRelationBySystemRoleId(SystemRoleID)) {
                if (! roleRelation.getIsDeleted())
                    list.add(roleRelation);
            }
            return list;
        }
        else
            throw new ETDException(102, 00, "Ошибка при получении элемента: не найден");
    }

    @RequestMapping(value="/api/getUserRoleRelations/{UserRoleID}")
    @ResponseBody
    public List<RoleRelation> getUserRoleRelations(@PathVariable("UserRoleID") Long UserRoleID) throws ETDException {

        List<RoleRelation> list = new ArrayList<RoleRelation>();


        Optional<UserRole> result = userRoleRepository.findById(UserRoleID);
        if (result.isPresent()) {
            for (RoleRelation roleRelation : roleRelationRepository.findRoleRelationByUserRoleId(UserRoleID)) {
                if (! roleRelation.getIsDeleted())
                    list.add(roleRelation);
            }
            return list;
        }
        else
            throw new ETDException(101, 00, "Ошибка при получении элемента: не найден");
    }



    @RequestMapping(value="/api/getRoleRelation/{RoleRelationID}")
    @ResponseBody
    public RoleRelation getRoleRelation(@PathVariable("RoleRelationID") Long RoleRelationID) throws Exception {
        return (RoleRelation)getEntity(roleRelationRepository, RoleRelationID, 103);
    }



    @RequestMapping(value="/api/updateRoleRelation", method = RequestMethod.POST, headers="Accept=*/*",  produces="application/json;; charset=UTF-8")
    @ResponseBody
    public RoleRelation updateRoleRelation(@RequestBody RoleRelation element)  throws Exception {

        RoleRelation roleRelation = null;

        RestControllerBase.ACTION action = ACTION.NEW_ELEMENT; //создание
        if (element.getId()!=null) {
            Optional<RoleRelation> findResult = roleRelationRepository.findById(element.getId());
            if (findResult.isPresent()) {
                action = ACTION.MODIFY_ELEMENT;
                roleRelation = findResult.get();
                if (roleRelation.getIsDeleted() == true)
                    throw new ETDException(106, 00, "Ошибка при модификации элемента " + element.getId());

                // при обновлении нельзя менять UserRole и SystemRole - их менять только созданием новой записи, перезаписываю старыми значениями
                element.setUserRoleId(roleRelation.getUserRoleId());
                element.setSystemRoleId(roleRelation.getSystemRoleId());
            }
        }
        else{ //если новая связка - проверить на дубль
            List<RoleRelation> existRelations =  roleRelationRepository.findRoleRelationsByUserRoleIdAndSystemRoleIdAndIsDeleted(element.getUserRoleId(), element.getSystemRoleId(), false);
            if (existRelations.size()>0)

                throw new ETDException(107, 00, "Ошибка при модификации элемента " + element.getId());
        }

        //проверить на удаленные или несуществующие роли - нельзя связывать с удаленными ролями
        UserRole userRole = userRoleRepository.getByIdAndIsDeleted(element.getUserRoleId(), false);
        SystemRole systemRole = systemRoleRepository.getByIdAndIsDeleted(element.getSystemRoleId(), false);


        if (userRole == null || systemRole == null)
            throw new ETDException(103, 00, "Ошибка при модификации элемента. Не указана роль. " + element.getId());


        return (RoleRelation) saveEntity(roleRelationRepository, element, 107);
    }


    @RequestMapping(value="/api/updateUserRoleRelations", method = RequestMethod.POST, headers="Accept=*/*",  produces="application/json;; charset=UTF-8")
    @ResponseBody
    public List<RoleRelation> updateUserRoleRelations(@RequestBody UserRoleRelationBulkChange bulkChangeElements) throws Exception{


        List<RoleRelation> list = new ArrayList<RoleRelation>();

        //удаление массива связок
        for (Long deletedID: bulkChangeElements.deletedRelations){
            
            list.add(deleteRoleRelation(deletedID));
        }

        //создание по массиву связок
        for (RoleRelation roleRelation: bulkChangeElements.addedRelations) {
            try {
                list.add(updateRoleRelation(roleRelation));
            }
            catch (Exception ex)
            {}
        }

        return  list;
    }





    @RequestMapping(value="/api/deleteRoleRelation/{RoleRelationID}")
    @ResponseBody
    public RoleRelation deleteRoleRelation(@PathVariable("RoleRelationID") Long RoleRelationID) throws Exception{
        //TO DO - сделать каскадное удаление элементов - удалили роль - удалить связи с ней. записать историю об этом
        return (RoleRelation) deleteEntity(roleRelationRepository, RoleRelationID, 106);
    }

    @RequestMapping(value="/api/getRoleRelationHistory/{RoleRelationID}")
    @ResponseBody
    public List<HistoryItem> getUserRoleHistory(@PathVariable("RoleRelationID") Long roleRelationID) throws Exception{
        return historyItemRepository.getAllByRoleRelationId(roleRelationID);
    }
}
