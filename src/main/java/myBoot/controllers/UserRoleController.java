package myBoot.controllers;

import myBoot.ETDException;
import myBoot.models.HistoryChange;
import myBoot.models.HistoryItem;
import myBoot.models.UserRole;
import myBoot.models.baseEntity;
import myBoot.repository.HistoryChangeRepository;
import myBoot.repository.HistoryItemRepository;
import myBoot.repository.UserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.jws.soap.SOAPBinding;
import java.util.*;

@Controller
public class UserRoleController extends RestControllerBase {

    @Autowired UserRoleRepository userRoleRepository;
    @Autowired HistoryItemRepository historyItemRepository;



    @RequestMapping(value="/api/getUserRoles")
    @ResponseBody
    public List<UserRole> getUserRoles() throws Exception{
        return getEntityList(userRoleRepository, UserRole.class);
    }



    @RequestMapping(value="/api/getUserRole/{UserRoleID}")
    @ResponseBody
    public UserRole getUserRole(@PathVariable("UserRoleID") Long UserRoleID) throws Exception {
        return (UserRole)getEntity(userRoleRepository, UserRoleID, 101);
     }



    @RequestMapping(value="/api/updateUserRole", method = RequestMethod.POST, headers="Accept=*/*",  produces="application/json;; charset=UTF-8")
    @ResponseBody
    public UserRole updateUserRole(@RequestBody UserRole element)  throws Exception {
        return (UserRole) saveEntity(userRoleRepository, element, 104);
    }

    @RequestMapping(value="/api/deleteUserRole/{UserRoleID}")
    @ResponseBody
    public UserRole deleteUserRole(@PathVariable("UserRoleID") Long UserRoleID) throws Exception{
        //TO DO - сделать каскадное удаление элементов - удалили роль - удалить связи с ней. записать историю об этом
        return (UserRole) deleteEntity(userRoleRepository, UserRoleID, 104);
    }



    @RequestMapping(value="/api/getUserRoleHistory/{UserRoleID}")
    @ResponseBody
    public List<HistoryItem> getUserRoleHistory(@PathVariable("UserRoleID") Long userRoleID) throws Exception{
        return historyItemRepository.getAllByUserRoleId(userRoleID);
    }

}
