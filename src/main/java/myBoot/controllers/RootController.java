package myBoot.controllers;

import myBoot.models.UserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Controller
public class RootController {


    @Autowired
    public RequestMappingHandlerMapping requestMappingHandlerMapping;
    //@RequestMapping(value="/api/updateUserRoleRelations", method = RequestMethod.POST, headers="Accept=*/*",  produces="application/json;; charset=UTF-8")
    @RequestMapping(value="/endpoints", produces="text/html; charset=UTF-8")
    public @ResponseBody
    Object showEndpointsAction() throws Exception
    {
        String result = "<html>";

        for (Object o:
        requestMappingHandlerMapping.getHandlerMethods().keySet().stream().map(t ->
                (t.getMethodsCondition().getMethods().size() == 0 ? "GET" : t.getMethodsCondition().getMethods().toArray()[0]) + " " +
                        t.getPatternsCondition().getPatterns().toArray()[0]
        ).toArray()){
            result += o.toString()+"<br>\r\n";
        }

        return result+"</html>";
    }

    @RequestMapping("/")
    String index() {
        return "index";
    }


    @RequestMapping("/reestr")
    String reestr() {
        return "reestr";
    }


    @RequestMapping("/user_roles")

    String roles() {


        return "user_roles";
    }



    @RequestMapping("/permissions")
    String permissions() {
        return "permissions";
    }




}
