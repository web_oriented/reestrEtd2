package myBoot.controllers;



import myBoot.models.RoleRelation;
import myBoot.models.SystemRole;
import myBoot.models.UserRole;
import myBoot.models.UserRoleRelationBulkChange;
import myBoot.repository.RoleRelationRepository;
import myBoot.repository.SystemRoleRepository;
import myBoot.repository.UserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import javax.xml.crypto.dsig.keyinfo.KeyValue;
import java.lang.reflect.Array;
import java.util.*;

@Controller
public class TestController extends RestControllerBase {



    public class Pair<F, S> extends java.util.AbstractMap.SimpleImmutableEntry<F, S> {

        public  Pair( F f, S s ) {
            super( f, s );
        }

        public F getFirst() {
            return getKey();
        }

        public S getSecond() {
            return getValue();
        }

        public String toString() {
            return "["+getKey()+","+getValue()+"]";
        }

    }


    @Autowired
    UserRoleRepository userRoleRepository;

    @Autowired
    RoleRelationRepository roleRelationRepository;

    @Autowired
    SystemRoleRepository systemRoleRepository;


    @RequestMapping("/api/runTests")
    @ResponseBody
    public ArrayList<?> runTests(){



        ArrayList<Pair> testResultSet = new ArrayList<>();

        /*  тесты:
            создать по 20 ролей (user & system).
            удалить первые 10
            модифицировать 5(ошибка) и 15 (успех)

            создать  связь с удаленными ролями (ошибка)
            создать связей с живыми ролями 6 7
            создать 100 связей с живыми ролями (bulk ) 10+ .. 10+


            удалить первые 20 связей
            просмотр всех элементов, загруженых в базу
            просмотр элементов которых нет в базе
        * */


        for (Integer i=1; i< 21; i++){
            UserRole newUserRole = new UserRole();
            newUserRole.setComment("newUserRole"+i);
            newUserRole.setJiraIssue("Jira"+i);
            newUserRole.setRoleNumber(i);
            testResultSet.add(new Pair<String, UserRole>("/api/updateUserRole/", testPostObject(UserRole.class,"/api/updateUserRole/", newUserRole)));

            SystemRole newSystemRole = new SystemRole();
            newSystemRole.setComment("newSystemRole"+i);
            newSystemRole.setJiraIssue("Jira"+i);
            newSystemRole.setRoleNumber(i);
           testResultSet.add(new Pair<String, SystemRole>("/api/updateSystemRole/", testPostObject(SystemRole.class,"/api/updateSystemRole/", (Object)newSystemRole)));
        }


        for (Integer i=1; i< 11; i++){
            testResultSet.add(new Pair<String, UserRole>("/api/deleteUserRole/"+i, testGet(UserRole.class,"/api/deleteUserRole/", i.toString())));
            testResultSet.add(new Pair<String, SystemRole>("/api/deleteSystemRole/"+i, testGet(SystemRole.class,"/api/deleteSystemRole/", i.toString())));
        }

        //generate exception
        UserRole deletedUserRole = userRoleRepository.findById(5L).get();
        deletedUserRole.setJiraIssue("jira delete issue");
        testResultSet.add(new Pair<String, UserRole>("/api/updateUserRole/"+deletedUserRole.getId().toString(), testPostObject(UserRole.class,"/api/updateUserRole/", (Object)deletedUserRole)));

        UserRole normalUserRole = userRoleRepository.findById(15L).get();
        normalUserRole.setJiraIssue("jira modify user role issue");
        testResultSet.add(new Pair<String, UserRole>("/api/updateUserRole/"+normalUserRole.getId().toString(), testPostObject(UserRole.class,"/api/updateUserRole/", (Object)normalUserRole)));

        //generate exception
        SystemRole deletedSystemRole = systemRoleRepository.findById(5L).get();
        deletedSystemRole.setJiraIssue("jira delete issue");
        testResultSet.add(new Pair<String, SystemRole>("/api/updateSystemRole/"+deletedSystemRole.getId().toString(), testPostObject(SystemRole.class,"/api/updateSystemRole/", (Object)deletedSystemRole)));

        SystemRole normalSystemRole = systemRoleRepository.findById(15L).get();
        normalSystemRole.setJiraIssue("jira modify user role issue");
        testResultSet.add(new Pair<String, SystemRole>("/api/updateSystemRole/"+normalSystemRole.getId().toString(), testPostObject(SystemRole.class,"/api/updateSystemRole/", (Object)normalSystemRole)));

        //generate exception
        RoleRelation deletedRoleRelation = new RoleRelation();
        deletedRoleRelation.setUserRoleId(deletedUserRole.getId());
        deletedRoleRelation.setSystemRoleId(deletedSystemRole.getId());
        deletedRoleRelation.setJiraIssue("jira issue create deleted role relaion 5");
        testResultSet.add(new Pair<String, RoleRelation>("/api/updateRoleRelation/new", testPostObject(RoleRelation.class,"/api/updateRoleRelation/", (Object)deletedRoleRelation)));


        RoleRelation normaRoleRelation = new RoleRelation();
        normaRoleRelation.setUserRoleId(normalUserRole.getId());
        normaRoleRelation.setSystemRoleId(normalSystemRole.getId());
        normaRoleRelation.setJiraIssue("jira issue create normal role relaion 15");
        testResultSet.add(new Pair<String, RoleRelation>("/api/updateRoleRelation/new", testPostObject(RoleRelation.class,"/api/updateRoleRelation/", (Object)normaRoleRelation)));




        UserRoleRelationBulkChange bulkChange = new UserRoleRelationBulkChange();
        for (long i=11; i< 13; i++){
            UserRole userRole = userRoleRepository.findById(i).get();
            for (long j=11; j< 13; j++){
                SystemRole systemRole = systemRoleRepository.findById(j).get();


                RoleRelation newRoleRelation = new RoleRelation();
                newRoleRelation.setUserRoleId(userRole.getId());
                newRoleRelation.setSystemRoleId(systemRole.getId());
                newRoleRelation.setJiraIssue("jira issue bulk create role relaion "+ i +" "+ j );

                bulkChange.addedRelations.add(newRoleRelation);
            }
        }

        testResultSet.add(new Pair<String, UserRoleRelationBulkChange>("/api/updateUserRoleRelations/new", testPostObject(UserRoleRelationBulkChange.class,"/api/updateUserRoleRelations/", (Object)bulkChange)));



/*
        testResultSet.add(new Pair<String, UserRole>("/api/getUserRole/"+userRole.getId().toString(), testGet(UserRole.class,"/api/getUserRole/", userRole.getId().toString())));


        testResultSet.add(new Pair<String, SystemRole>("/api/getSystemRole/"+systemRole.getId().toString(), testGet(SystemRole.class,"/api/getSystemRole/", systemRole.getId().toString())));
        testResultSet.add(new Pair<String, RoleRelation>("/api/getRoleRelation/"+roleRelation.getId().toString(), testGet(RoleRelation.class,"/api/getRoleRelation/", roleRelation.getId().toString())));


        testResultSet.add(new Pair<String, UserRole>("/api/getUserRole/777", testGet(UserRole.class,"/api/getUserRole/", "777")));
        testResultSet.add(new Pair<String, SystemRole>("/api/getSystemRole/777", testGet(SystemRole.class,"/api/getSystemRole/", "777")));
        testResultSet.add(new Pair<String, RoleRelation>("/api/getRoleRelation/777", testGet(RoleRelation.class,"/api/getRoleRelation/", "777")));


        testResultSet.add(new Pair<String, List>("/api/getUserRoles/", testGet(List.class,"/api/getUserRoles/", null)));
        testResultSet.add(new Pair<String, List>("/api/getSystemRoles/", testGet(List.class,"/api/getSystemRoles/", null)));
        testResultSet.add(new Pair<String, List>("/api/getSystemRoleRelations/"+systemRole.getId().toString(), testGet(List.class,"/api/getSystemRoleRelations/", systemRole.getId().toString())));
        testResultSet.add(new Pair<String, List>("/api/getUserRoleRelations/"+userRole.getId().toString(), testGet(List.class,"/api/getUserRoleRelations/", userRole.getId().toString())));
*/
/*

        userRole.setComment(userRole.getComment()+"_1");
        testResultSet.add(new Pair<String, UserRole>("/api/updateUserRole/"+userRole.getId().toString(), testPostObject(UserRole.class,"/api/updateUserRole/", (Object)userRole)));

        systemRole.setComment(systemRole.getComment()+"_2");
        testResultSet.add(new Pair<String, SystemRole>("/api/updateSystemRole/"+systemRole.getId().toString(), testPostObject(SystemRole.class,"/api/updateSystemRole/", (Object)systemRole)));

        roleRelation.setJiraIssue(roleRelation.getJiraIssue()+"_3");
        testResultSet.add(new Pair<String, RoleRelation>("/api/updateRoleRelation/"+roleRelation.getId().toString(), testPostObject(RoleRelation.class,"/api/updateRoleRelation/", (Object)roleRelation)));



        testResultSet.add(new Pair<String, List>("/api/getUserRoleHistory/"+userRole.getId().toString(), testGet(List.class,"/api/getUserRoleHistory/", userRole.getId().toString())));
        testResultSet.add(new Pair<String, List>("/api/getSystemRoleHistory/"+systemRole.getId().toString(), testGet(List.class,"/api/getSystemRoleHistory/", systemRole.getId().toString())));
        testResultSet.add(new Pair<String, List>("/api/getRoleRelationHistory/"+roleRelation.getId().toString(), testGet(List.class,"/api/getRoleRelationHistory/", roleRelation.getId().toString())));
*/




        return  testResultSet;
    }


    public <T> T testPostObject(Class<T> responseType, String path, Object object){
        final String uri = myServerURL+path;
        System.out.println("testPostObject: "+uri);
        T result = null;
        try {
            result= responseType.newInstance();
            RestTemplate restTemplate = new RestTemplate();

            result = restTemplate.postForObject(uri, (T) object, responseType);
        }
        catch (Exception ex){

        }

        return result;
    }



    public <T> T  testGet (  Class<T> responseType, String path, String param){

        String uri = myServerURL+path;
        if (param!=null)
            uri += "{param}";

        System.out.println("testGet: "+uri+(param!=null?", param="+param:""));

        T result = null;
        try {
            result= responseType.newInstance();

            RestTemplate restTemplate = new RestTemplate();





            Map<String, String> params = new HashMap<String, String>();
            if (param != null) {
                params.put("param", param);
                result = restTemplate.getForObject(uri, responseType, params);
            } else
                result = restTemplate.getForObject(uri, responseType);
        }
        catch (Exception ex){

        }

        return  result;
    }




}
